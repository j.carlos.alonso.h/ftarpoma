---
title: "Resumen"
date: 2017-11-18T22:46:36-03:00
draft: false
weight: -110
---

Este trabajo de memoria de título es una investigación que expondrá sobre el cómo poder ayudar al desempeño de diferentes personas que se encuentren en un proyecto ágil, Scrum.

Esta investigación demostrará como guiarse utilizando la propuesta de solución, con una aplicación y con una encuesta para diferentes personas y roles de trabajo; y validar su ayuda.

El resultado será una propuesta de solución y su análisis del desempeño del rol que protagoniza el Dueño del Producto, el cual es responsable de la visión del producto en el contexto mencionado anteriormente y como éste rol debe seguir los principios del manifiesto ágil, en base a Scrum y otras disciplinas similares combinadas.
