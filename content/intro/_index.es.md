---
title: "Introducción"
date: 2017-11-18T23:20:12-03:00
draft: false
weight: -100
---

Este documento describe cómo se debe generar un buen producto y el rol del responsable, quien es el encargado de seguir buenas prácticas y velar por la realización de la visión del producto. Además, se presenta el desarrollo de la creación de un producto y su relación con el desempeño de un equipo de personas que está cambiando su forma de trabajar en un período de tiempo. Se describe los variados problemas que se van encontrando cómo; intentar mantener la velocidad del equipo, también cómo el Scrum Master hace acrobacias para mantener estable el equipo de desarrollo y primordialmente como el Dueño del Producto hace su mejor trabajo ante la incertidumbre para entregar valor al cliente y satisfacer sus necesidades.

En el Capítulo 1 se describe sobre cómo llegar a la problemática, del por qué hacer "Toolkit de apoyo al rol de 'Product Owner'" para la creación de una solución y como llevarlo al contexto del desarrollo de software utilizando metodologías ágiles, Scrum.

El Capítulo 2 explica el estado del arte de las metodologías ágiles, con respecto a la función de "Dueño del Producto", cómo este papel debe ser interpretado o ejecutarse. Cómo se debe mantener las diferentes relaciones que tiene el "Product Owner" en relación a los Stakeholders, al equipo de trabajo y al ScrumMaster en torno (previo, durante y post) al del desarrollo del producto. También se indicarán las distintas técnicas que deben o podrían utilizar para permitir la creación de un Producto. En resumen, un breve listado del "Toolkit de apoyo al rol de 'Product Owner'" con una descripción.

El Capítulo 3 propone un conjunto de herramientas, artefactos y buenas prácticas que puede utilizar el "Product Owner". Este conjunto lo acompaña la necesidad de dar valor al negocio, con la conformación del equipo y roles que se deben asumir. Además, este conjunto propone dar el resultado de un esquema de trabajo o diseño inicial para trabajos de equipo para el desarrollo particular.

Capitulo 4 es una breve guía de 5 pasos de la implementación de la propuesta de solución; es un criterio de uso de las distintas herramientas que el "Product Owner" puede trabajar que dentro del proceso de su utilización de Scrum.

En el Capítulo 5 se recopila información de encuestas a diferentes profesionales enfocados en el contexto del Toolkit, indicando que herramientas funcionan y apoyen durante el proceso del desarrollo de sus proyectos y además un resumen del resultado de una simulación real.

En el último capítulo se dará las conclusiones y sugerencias de esta memoria.
